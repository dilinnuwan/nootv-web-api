<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if (empty(auth()->user())) {
		return view('welcome');
	}else{
		return redirect('/home');
	}
});

Route::get('/pricing', function () {
	if (empty(auth()->user())) {
		return view('pricing');
	}else{
		return redirect('/home');
	}
})->name('pricing');







Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/subscribe', 'SubscribeController@index')->name('subscribe');
Route::post('/subscribe', 'SubscribeController@store')->name('subscribe');

Route::get('/account', 'AccountController@index')->name('account');
Route::post('/name_update', 'AccountController@name_update')->name('name_update');
Route::post('/change_password', 'AccountController@change_password')->name('changePassword');


Route::get('/channel/{id}', 'ChannelController@index');
Route::get('/channel/{c_id}/{ep_id}', 'EpisodesController@index');


Route::get('/video/{id}', 'VideoController@index');
Route::get('/search/', 'SearchController@index');