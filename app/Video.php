<?php

namespace NOTV;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public function Channel()
    {
        return $this->belongsTo('NOTV\Channel','channel_id');
    }

    public function Series()
    {
        return $this->belongsTo('NOTV\Series');
    }
}
