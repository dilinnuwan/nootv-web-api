<?php

namespace NOTV;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    // public function videos()
    // {
    //     return $this->hasMany('NOTV\Video','channel_id');
    // }


    public function Series()
    {
        return $this->hasMany('NOTV\Series','channel_id');
    }
}
