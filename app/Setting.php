<?php

namespace NOTV;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    public function User()
    {
        return $this->belongsTo('NOTV\User'); // links this->course_id to courses.id
    }
}
