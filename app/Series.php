<?php

namespace NOTV;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    public function Channel()
    {
        return $this->belongsTo('NOTV\Channel'); // links this->course_id to courses.id
    }

    public function Videos()
    {
        return $this->hasMany('NOTV\Video','series_id');
    }
}
