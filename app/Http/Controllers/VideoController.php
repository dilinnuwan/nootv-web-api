<?php

namespace NOTV\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use NOTV\Video;
use NOTV\Series;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
    	$video = Video::find($id);

    	// $episodes = Series::find($video->Series->id);
    	$episodes = Video::where('channel_id',$video->channel_id)->where('series_id',$video->series_id)->orderBy('release_date', 'desc')->get();

    	// dd($episodes);


    	return view('user_views.video')->with(compact('video','episodes'));
    }
}
