<?php

namespace NOTV\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use NOTV\Channel;

class ChannelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
    	// dd(Channel::find($id)->Series);
    	$channel = Channel::find($id);

    	// dd($channel);
    	return view('user_views.channel')->with(compact('channel'));
    }
}
