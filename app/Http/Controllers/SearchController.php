<?php

namespace NOTV\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use NOTV\Video;

use Illuminate\Support\Facades\Input;


class SearchController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$keyword = Input::get ( 'keyword' );

    	if (!empty($keyword)) {

		    	$result = Video::where('name','LIKE','%'.$keyword.'%')->orWhere('tags','LIKE','%'.$keyword.'%')->paginate(25);
		    	$result->appends(['keyword' => $keyword]);

		    	if(count($result) > 0)
		        {
		        	return view('user_views.search')->withResults($result)->withQuery ( $keyword );
		        }
		    	else 
		    	{
		    		return view ('user_views.search')->withMessage('No Results found. Try to search again !');
		    	}

	    }
	    else{
	    	return view ('user_views.search');
	    }


    	// return view('user_views.search');
    }
}
