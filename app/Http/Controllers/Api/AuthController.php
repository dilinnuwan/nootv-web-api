<?php

namespace NOTV\Http\Controllers\Api;

use NOTV\User;
use NOTV\Channel;
use NOTV\Series;
use NOTV\Video;


use Laravel\Passport\Passport;
use Illuminate\Http\Request;
use NOTV\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);


        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;

        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function channel_list()
    {
        return response()->json(Channel::all());
    }

    public function series_list($id)
    {
        return response()->json(Channel::find($id)->Series);
    }
	
		public function episode_list($id)
    {
// 			->Videos->get()
			$videos = Video::where('series_id',$id)->orderBy('release_date', 'desc')->get();
//         return response()->json(Series::find($id)->Videos->orderBy('release_date', 'desc')->get());
			return response()->json($videos);
    }
	
		public function fetch_video($id){
				$fetch_data = Video::find($id);
			
				$fetch_next_video = Video::where('epsode_id',$fetch_data['epsode_id']+1)->get();
				$fetch_prev_video = Video::where('epsode_id',$fetch_data['epsode_id']-1)->get();
			
				$video["video_name"] = $fetch_data['name'];
				$video["video_url"] = $fetch_data['url'];
				$video["video_thumb"] = $fetch_data['thumb'];
				$video["series_id"] = $fetch_data->Series['id'];
				$video["series_name"] = $fetch_data->Series['name'];
			
				if(!empty($fetch_next_video[0])){
					$video["next_video_id"] = $fetch_next_video[0]['id'];
					$video["next_video_thumb"] = $fetch_next_video[0]['thumb'];
				}else{
					$video["next_video_id"] = null;
					$video["next_video_thumb"] = null;
				}
			
			
				if(!empty($fetch_prev_video[0])){
					$video["prev_video_id"] = $fetch_prev_video[0]['id'];
					$video["prev_video_thumb"] = $fetch_prev_video[0]['thumb'];
				}else{
					$video["prev_video_id"] = null;
					$video["prev_video_thumb"] = null;
				}
			
// 			return response()->json();
			return response()->json($video);
		}


}