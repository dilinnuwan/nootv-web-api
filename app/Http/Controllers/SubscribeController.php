<?php

namespace NOTV\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use NOTV\Plans;

class SubscribeController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {
    	$plans = Plans::all();
    	$user = Auth::user();
    	if ($user->subscribed('main')) {
    		return redirect('/home');
    	}else{
    		return view('user_views.subscribe')->with('plans',$plans);
    	}

    }

    public function store(Request $request)
    {
    	// $user = Auth::user();
    	// return view('user_views.subscribe');
    	// dd($request->plan_id);

    	
		$token =$request->stripeToken;
    	try {
    		auth()->user()->newSubscription('main',$request->plan)->create($token);
    		return redirect('/home');
    	} catch (Exception $e) {
    		return $e;
    	}
     

		// return redirect('/home');

    }


}
