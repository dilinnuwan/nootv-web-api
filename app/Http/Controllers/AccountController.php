<?php

namespace NOTV\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use Stripe\Stripe;
use Stripe\Customer;
use Hash;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$user = Auth::user();


    	// Retrieve the timestamp from Stripe
		$Stripe = $user -> asStripeCustomer()["subscriptions"] -> data[0];
		// Cast to Carbon instance and return
		$next_billing = \Carbon\Carbon::createFromTimeStamp($Stripe["current_period_end"]) -> toFormattedDateString();


		Stripe::setApiKey(env('STRIPE_SECRET'));
		$customer = Customer::retrieve($Stripe['customer']);
		$card = Customer::retrieveSource($customer['id'],$customer['default_source']);
		// dd($card);

    	 return view('user_views.account', compact('user','next_billing','card'));
    }

    public function name_update(Request $request)
    {
    	$user = Auth::user();
    	$user->name = $request->firstname;
    	$user->save();

    	return redirect(route('account'));
    }


    public function change_password(Request $request)
    {
    	// dd($request);
    	if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

     //    //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }
}
