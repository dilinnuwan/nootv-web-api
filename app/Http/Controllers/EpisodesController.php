<?php

namespace NOTV\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use NOTV\Series;
use NOTV\Video;

class EpisodesController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($c_id,$ep_id)
    {

    	$episodes = Video::where('channel_id',$c_id)->where('series_id',$ep_id)->orderBy('release_date', 'desc')->paginate(25);

    	// dd($episodes);

    	$series = Series::find($ep_id);
    	$series->views = $series->views+1;
    	$series->save();


    	// dd($episodes);
    	return view('user_views.episodes')->with(compact('episodes','series'));
    }
}
