-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 16, 2020 at 12:18 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nootv`
--

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_logo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `channel_name`, `channel_logo_url`) VALUES
(1, 'Derana Tv', 'https://notv.sgp1.digitaloceanspaces.com/Channel_logo/derana.png'),
(2, 'Sirasa Tv', 'https://notv.sgp1.digitaloceanspaces.com/Channel_logo/sirasa.png'),
(3, 'Hiru Tv', 'https://notv.sgp1.digitaloceanspaces.com/Channel_logo/hirutv.png'),
(4, 'Rupavahini', 'https://notv.sgp1.digitaloceanspaces.com/Channel_logo/rupavahini.png');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `device_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `device_name`, `device_code`, `created_at`, `updated_at`) VALUES
(1, 'TvOs', 'Hnskkwhu', NULL, NULL),
(2, 'Android', 'scsweASc', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `device_user`
--

CREATE TABLE `device_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `device_user`
--

INSERT INTO `device_user` (`id`, `user_id`, `device_id`) VALUES
(1, 6, 2),
(2, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_01_03_031823_settings', 2),
(10, '2020_01_03_031939_videos', 3),
(13, '2020_01_04_044429_create_device_user_table', 6),
(14, '2020_01_03_031915_devices', 7),
(15, '2020_01_13_145533_create_channels_table', 8),
(16, '2020_01_14_202737_create_series_table', 9),
(17, '2020_02_03_035816_subscriptions', 10),
(18, '2014_10_12_000000_create_users_table', 11),
(19, '2020_02_03_193842_create_permission_tables', 12),
(20, '2020_02_04_202524_create_plans_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('088dc1415b8437474f0ef74cf955c8192d01df0d8e5a8a44b8e1eb68d3bbb61a92f7e010375a8a61', 4, 1, 'authToken', '[]', 0, '2020-01-03 02:12:16', '2020-01-03 02:12:16', '2021-01-02 21:12:16'),
('2c6a13c7927647c47c606f58f00abf0d5393c597c6b5a07d458a0b10ccc92498f50a9235df3dd502', 6, 1, 'Personal Access Token', '[]', 0, '2020-01-04 07:54:15', '2020-01-04 07:54:15', '2021-01-04 02:54:15'),
('5bb0d531674bd85c54b984431543a5a4b4f7c2a5af923fa1a0fa752c0654977252eaad1306001ebd', 6, 1, 'Personal Access Token', '[]', 0, '2020-01-04 07:52:37', '2020-01-04 07:52:37', '2021-01-04 02:52:37'),
('5d19eaecb34f57268f47ca3c1a942ac38f99fee0ca4c65297428f3a62afe7bd81b04f3fa4cf7952b', 1, 1, 'authToken', '[]', 0, '2020-01-02 10:39:06', '2020-01-02 10:39:06', '2021-01-02 05:39:06'),
('b31e99b5779299640e74d5df44ece54a43499b0225329eeb467d3aba698d25fa8dd8d20ed02a2680', 6, 1, 'Personal Access Token', '[]', 0, '2020-01-04 07:49:06', '2020-01-04 07:49:06', '2021-01-04 02:49:06'),
('c3531a35f845038173b2b52ac400c3d1553999d5e315c71b259ba5e483e1712a3c0c164d149757f6', 4, 1, 'authToken', '[]', 0, '2020-01-03 02:11:40', '2020-01-03 02:11:40', '2021-01-02 21:11:40'),
('d2c0d0b6ed6ef7fb4cd0e3d031cac5937016a5842022037738a118b241e9df24b09f67ca67afccde', 5, 1, 'Personal Access Token', '[]', 1, '2020-01-03 03:05:39', '2020-01-03 03:05:39', '2021-01-02 22:05:39'),
('ff2bb6cec07227405ae23bdc8c08b405bc8a5ac7fc2fa0f1ad49d4717210d938a807e8d4acae171d', 3, 1, 'authToken', '[]', 0, '2020-01-02 10:40:53', '2020-01-02 10:40:53', '2021-01-02 05:40:53');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'wWqKiv8v7ldRCUoUTYopdn4bHHNmMfnlDfZlaO4D', 'http://localhost', 1, 0, 0, '2020-01-02 07:29:47', '2020-01-02 07:29:47'),
(2, NULL, 'Laravel Password Grant Client', 'WLMUdcCHED1Imeprod0AzSHwO3y6ND5hkZKZNLHj', 'http://localhost', 0, 1, 0, '2020-01-02 07:29:47', '2020-01-02 07:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-01-02 07:29:47', '2020-01-02 07:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('dilinanuwan@gmail.com', '$2y$10$ZyAy5o94WT.bSaOPx/.vqeSEOR7Tz7zctOzQXmPIWJzBNJpZ4NuhC', '2020-01-30 07:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `stripe_plan_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_product_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_5` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `feature_6` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name`, `price`, `stripe_plan_id`, `stripe_product_id`, `feature_1`, `feature_2`, `feature_3`, `feature_4`, `feature_5`, `feature_6`, `created_at`, `updated_at`) VALUES
(1, 'Monthly', 799, 'plan_GfIo75E9c5jz6m', 'prod_GfInSK6LI1pKWS', 'First Month Free', '$7.99+tax Per Month', 'Full HD', 'TV & Desktop', '24/7 Support', '', '2020-02-04 08:00:00', '2020-02-04 08:00:00'),
(2, 'Anually', 9499, 'plan_GfIoybPFdurH3H', 'prod_GfInSK6LI1pKWS', 'First Month Free', '$94.99+tax Per Month', 'Full HD', 'TV & Desktop', '24/7 Support', '', '2020-02-04 08:00:00', '2020-02-04 08:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `series`
--

CREATE TABLE `series` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT NULL,
  `channel_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `series`
--

INSERT INTO `series` (`id`, `name`, `thumb_url`, `age`, `views`, `channel_id`, `created_at`, `updated_at`) VALUES
(1, 'Derana News', 'https://notv.sgp1.digitaloceanspaces.com/Derana/News/Thumbs/derana_news_thumb.png', NULL, 7, 1, NULL, '2020-02-15 08:32:16'),
(2, 'Deweni Inima', NULL, NULL, 12, 1, NULL, '2020-02-09 08:29:54'),
(3, 'Pandith Rama', NULL, NULL, 14, 1, NULL, '2020-02-09 08:29:57');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_quality` int(11) NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `user_id`, `video_quality`, `avatar`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
(1, 5, 480, '', 'Dilina', 'Guna', NULL, NULL),
(2, 6, 1080, '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subscriptions`
--

INSERT INTO `subscriptions` (`id`, `user_id`, `name`, `stripe_id`, `stripe_plan`, `quantity`, `trial_ends_at`, `ends_at`, `created_at`, `updated_at`) VALUES
(9, 2, 'main', 'sub_GgjDwY19xkBQfU', 'plan_GfIo75E9c5jz6m', 1, NULL, NULL, '2020-02-07 06:36:10', '2020-02-07 06:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stripe_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_last_four` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'User',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `stripe_id`, `card_brand`, `card_last_four`, `trial_ends_at`, `active`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Dilina', 'dilinanuwan@gmail.com', '$2y$10$5LrcVmh/a0pKa6lp3l6mcuj6hZqg3iWmoF.qjAdk49g/Q4Y/Vkk8e', NULL, NULL, NULL, NULL, 0, 'User', 'Is2pHHQ55LbQQP0W9huM23I3v8uaxbEF74z1w3CxJnpFbeTTJUCZura0meid', '2020-02-03 09:50:02', '2020-02-04 00:01:29'),
(2, 'DikanKnight', 'dikanknight@gmail.com', '$2y$10$vYcADuvVPpazohKAIEW0wudSkVIX.eJONoLZvPnmNB/yw7uiWB72e', 'cus_GfvwqprkR63LFO', 'Visa', '4242', NULL, 0, 'User', 'BKANLpyv0M85ztNlfTNzAPeHtzON3ccllAcOIxTZqGSDoOIE0dXOWwWenxJ6', '2020-02-03 09:51:18', '2020-02-08 00:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_device`
--

CREATE TABLE `user_device` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_device`
--

INSERT INTO `user_device` (`id`, `user_id`, `device_id`) VALUES
(1, 6, 2);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `release_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `epsode_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `channel_id` int(11) DEFAULT NULL,
  `series_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `name`, `thumb`, `url`, `release_date`, `tags`, `epsode_id`, `language_id`, `channel_id`, `series_id`, `created_at`, `updated_at`) VALUES
(1, 'Ada Derana News', 'https://notv.sgp1.digitaloceanspaces.com/Derana/News/Thumbs/derana_news_thumb.png', 'https://notv.sgp1.digitaloceanspaces.com/Derana/News/2020.01.12.mp4', '1578805200', 'news , derana, 6.50 , prime time news,sinhala', 100, 1, 1, 1, '2020-01-12 23:55:00', '2020-01-12 23:55:00'),
(2, 'Ada Derana News', 'https://notv.sgp1.digitaloceanspaces.com/Derana/News/Thumbs/derana_news_thumb.png', 'https://notv.sgp1.digitaloceanspaces.com/Derana/News/2020.01.13.mp4', '1578891600', 'news , derana, 6.50 , prime time news,sinhala', 101, 1, 1, 1, '2020-01-13 23:55:00', '2020-01-13 23:55:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_user`
--
ALTER TABLE `device_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_device`
--
ALTER TABLE `user_device`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `device_user`
--
ALTER TABLE `device_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `series`
--
ALTER TABLE `series`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_device`
--
ALTER TABLE `user_device`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
