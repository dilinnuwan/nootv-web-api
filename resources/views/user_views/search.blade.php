@extends('layouts.userlayout')

@section('content')
<!-- page title -->
<section class="section section--first section--bg" data-bg="img/section/section.jpg">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__wrap">
					<!-- section title -->
					<h2 class="section__title">Search</h2>
					<!-- end section title -->

					<!-- end breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end page title -->

<section class="section" data-bg="img/section/section.jpg" style="padding-top: 10px;padding-bottom: 0px">
<!-- content -->
<div class="content">

	<div class="container">
		<div class="row">
			<!-- details form -->
			<div class="col-12 col-lg-12">
				<form action="/search/" method="get" class="profile__form">
					{{-- {{ csrf_field() }} --}}
					<div class="row">

						<div class="col-8 col-md-9 col-lg-10 col-xl-10">
							<div class="profile__group">
								<input type="text" class="profile__input" placeholder="Search" name="keyword">
							</div>
						</div>

						<div class="col-4 col-md-3 col-lg-2 col-xl-2" >
							<button class="profile__btn" style="margin-top: 0px;width: 100%;background-color: #aa0001" type="submit"><i class="icon ion-ios-search"></i> &nbsp;&nbsp; Search</button>
						</div>


					</div>
				</form>
			</div>
			<!-- end details form -->
		</div>


		<!-- end content tabs -->
	</div>
</div>
<!-- end content -->
</section>


<!-- catalog -->
<div class="catalog" style="padding: 0px;">
	<div class="container">

		@if(isset($results))
		<div class="row">
			<p style="color: #fff;margin-bottom: 80px;margin-left: 20px">The Search results for your query <b> " {{ $query }} " </b> are :</p><br />
		</div>
		<div class="row">
			
				
				@foreach($results as $result)
				<!-- card -->
				<div class="col-6 col-sm-3 col-md-3 col-xl-3">
					<div class="card">
						<div class="card__cover">
							<img src="{{$result->thumb or asset('img/sample_thumb.jpg') }}" alt="">
							<a href="/video/{{$result->id}}" class="card__play">
								<i class="icon ion-ios-play"></i>
							</a>
						</div>
						<div class="card__content">
							<h3 class="card__title"><a href="/video/{{$result->id}}">{{$result->name}} ({{$result->epsode_id}})</a></h3>
						</div>
						<span class="card__category" style="color:#ccc;font-size: 12px;padding-top: 0px">
							{{ \Carbon\Carbon::createFromTimeStamp($result->release_date) -> toFormattedDateString() }}
						</span>
					</div>
				</div>
				<!-- end card -->
				@endforeach
			

			{{-- {{dd(Request::get('keyword'))}} --}}

			{{ $results->appends(['keyword' => Request::get('keyword')])->links() }}

			
		</div>
		@elseif(isset($message))
			<div class="col-12 centered" style="padding: 100px">
				<span style="color:#ccc;font-size: 20px">{{$message}}</span>
			</div>
		@else
			<div class="col-12 centered" style="padding: 100px">
				{{-- <span style="color:#ccc;font-size: 20px">{{$message}}</span> --}}
			</div>
		@endif
	</div>
</div>
<!-- end catalog -->




{{-- <div class="section">
	<div class="container">
	    @if(isset($results))
	        <p> The Search results for your query <b> {{ $query }} </b> are :</p>
	    <h2>Sample User results</h2>
	    <table class="table table-striped">
	        <thead>
	            <tr>
	                <th>Name</th>
	                <th>Email</th>
	            </tr>
	        </thead>
	        <tbody>
	            @foreach($results as $result)
	            <tr>
	                <td>{{$result->name}}</td>
	                <td>{{$result->tags}}</td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>
	    @endif
	</div>
</div> --}}



@endsection