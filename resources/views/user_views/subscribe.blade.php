@extends('layouts.userlayout')

@section('content')

<!-- page title -->
<section class="section section--first section--bg" data-bg="img/section/section.jpg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__wrap">
                    <!-- section title -->
                    <h2 class="section__title">Pricing plans</h2>
                    <!-- end section title -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end page title -->

<!-- pricing -->
<div class="section">
    <div class="container">
        <div class="row justify-content-center">
            <!-- plan features -->

            <!-- end plan features -->

            <!-- price -->
            {{-- <div class="col-0 col-md-0 col-lg-2">
            </div> --}}
            <!-- end price -->

            <!-- price -->
            @foreach($plans as $plan)
            <div class="col-12 col-md-6 col-lg-4">
                <div class="price price">
                    <div class="price__item price__item--first"><span>{{$plan->name}}</span></div>
                    <div class="price__item"><span>{{$plan->feature_1}}</span></div>
                    <div class="price__item"><span>{{$plan->feature_2}}</span></div>
                    <div class="price__item"><span>{{$plan->feature_3}}</span></div>
                    <div class="price__item"><span>{{$plan->feature_4}}</span></div>
                    <div class="price__item"><span>{{$plan->feature_5}}</span></div>
                    <div class="price_item right" style="margin-left: auto">
                        <span style="color: #fff;font-weight: bolder;font-size: 50px;">$ {{$plan->price/100}}</span>
                        <span style="color: #fff;font-size: 10px;"> /{{$plan->name}}</span>
                    </div>
                    {{-- <a href="{{ route('subscribe') }}" class="price__btn">Choose Plan</a> --}}
                </div>
            </div>
            @endforeach
            <!-- end price -->

            <div class="col-12 col-md-12 col-lg-4">
                <div class="price price">
                    <div class="price__item price__item--first"><span>Payment</span></div>
                    <form method="post" action="/subscribe" id="payment-form" style="width: 100%">
                        {{csrf_field()}}
                         <div style="width: 40%;display: inline;">
                            <div class="card-header">
                                    <label for="card-element" style="color:#999">
                                        Select subscription plan
                                    </label>
                                </div>
                            <select name="plan" class="form-group custom-select">
                                @foreach($plans as $plan)
                                      <option value="{{$plan->stripe_plan_id}}">{{$plan->name}} - ${{$plan->price/100}}</option>
                                @endforeach
                            </select>
                            
                        </div>

                        <div class="form-group">
                            <div class="card-header">
                                <label for="card-element" style="color:#999">
                                    Enter your credit card information
                                </label>
                            </div>
                            <div class="card-body">
                                <div id="card-element">
                                <!-- A Stripe Element will be inserted here. -->
                                </div>
                                <!-- Used to display form errors. -->
                                <div id="card-errors" role="alert"></div>
                                {{-- <input type="hidden" name="plan" value="plan_GfIoybPFdurH3H" /> --}}
                            </div>
                        </div>

                        {{-- <div class="form-group" style="margin-top: 20px">
                                <div class="card-header">
                                    <label for="card-element" style="color:#999">
                                        Enter your Coupon (Optional)
                                    </label>
                                </div>
                            <div class="sign__group">
                                <input name="coupon" class="form-group custom-select" name="coupon" type="text" placeholder="Coupon (Optional)">
                            </div>
                            
                        </div> --}}

                        <div class="card-footer">
                            <button class="price__btn" type="submit">Subscribe Now</button>
                        </div>
                    </form>
                    <div style="margin-top: 10px">
                        <img src="/img/cards.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end pricing -->



<style type="text/css">
    .StripeElement {
  box-sizing: border-box;

  height: 40px;

  padding: 10px 12px;

  border: 1px solid transparent;
  border-radius: 4px;
  background-color: white;

  box-shadow: 0 1px 3px 0 #e6ebf1;
  -webkit-transition: box-shadow 150ms ease;
  transition: box-shadow 150ms ease;
}

.StripeElement--focus {
  box-shadow: 0 1px 3px 0 #cfd7df;
}

.StripeElement--invalid {
  border-color: #fa755a;
}

.StripeElement--webkit-autofill {
  background-color: #fefde5 !important;
}

#card-errors{
    color:#ff0000!important;
}



/* The container must be positioned relative: */
.custom-select {
  /*position: relative;
  font-family: Arial;*/
  width: 100%;
  border-radius: 5px;
  padding: 6px;
  height: 40px
}


</style>
<script src="https://js.stripe.com/v3/"></script>
<script>
    // Create a Stripe client.
var stripe = Stripe('{{ env("STRIPE_KEY") }}');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  // base: {
  //   color: '#fff',
  //   lineHeight: '18px',
  //   fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
  //   fontSmoothing: 'antialiased',
  //   fontSize: '16px',
  //   '::placeholder': {
  //     color: '#fff'
  //   }
  // },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }

};

// Create an instance of the card Element.
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function(event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('payment-form');
form.addEventListener('submit', function(event) {
  event.preventDefault();

  stripe.createToken(card).then(function(result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      // Send the token to your server.
      stripeTokenHandler(result.token);
    }
  });
});

// Submit the form with the token ID.
function stripeTokenHandler(token) {
  // Insert the token ID into the form so it gets submitted to the server
  var form = document.getElementById('payment-form');
  var hiddenInput = document.createElement('input');
  hiddenInput.setAttribute('type', 'hidden');
  hiddenInput.setAttribute('name', 'stripeToken');
  hiddenInput.setAttribute('value', token.id);
  form.appendChild(hiddenInput);

  // Submit the form
  form.submit();
}
</script>
@endsection