@extends('layouts.userlayout')

@section('content')

<!-- page title -->
<section class="section section--first section--bg" data-bg="{{ asset('img/section/section.jpg') }} ">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__wrap">
					<!-- section title -->
					<h2 class="section__title">{{$channel->channel_name}}</h2>
					<!-- end section title -->

					<!-- breadcrumb -->
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{ route('home') }}">Home</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">{{$channel->channel_name}}</li>
					</ul>
					<!-- end breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end page title -->

<!-- catalog -->
<div class="catalog">
	<div class="container">
		<div class="row">


			@if(!$channel->Series->isEmpty())
				@foreach($channel->Series as $series)
				<!-- card -->
				<div class="col-6 col-sm-4 col-md-4 col-xl-4">
					<div class="card">
						<div class="card__cover">
							<img src="{{$series->thumb_url or asset('img/sample_thumb.jpg') }}" alt="">
							<a href="/channel/{{ $channel->id }}/{{$series->id}}" class="card__play">
								<i class="icon ion-ios-play"></i>
							</a>
						</div>
						<div class="card__content">
							<h3 class="card__title"><a href="/channel/{{ $channel->id }}/{{$series->id}}">{{$series->name}}</a></h3>
							<span class="card__category" style="color:#ccc;font-size: 12px;padding-top: 0px">
								Views - {{$series->views or '0'}}
							</span>
						</div>
					</div>
				</div>
				<!-- end card -->
				@endforeach
			@else
				<div class="col-12 centered">
					<span style="color:#ccc;font-size: 20px">No Results</span>
				</div>
				
			@endif

			<!-- paginator -->
			{{-- {{ $series->links() }} --}}
			<!-- end paginator -->
		</div>
	</div>
</div>
<!-- end catalog -->

@endsection