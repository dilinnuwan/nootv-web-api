@extends('layouts.userlayout')

@section('content')
	<!-- page title -->
	<section class="section section--first section--bg" data-bg="img/section/section.jpg">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="section__wrap">
						<!-- section title -->
						<h2 class="section__title">My Account</h2>
						<!-- end section title -->

						<!-- end breadcrumb -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end page title -->

	<!-- content -->
	<div class="content">
		<!-- profile -->
		<div class="profile">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="profile__content">
							<div class="profile__user">
								<div class="profile__avatar">
									<img src="img/user.png" alt="">
								</div>
								<div class="profile__meta">
									<h3>{{$user->name}}</h3>
									<span>{{$user->email}}</span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- end profile -->

		<div class="container">
			<div class="row">
				<!-- details form -->
				<div class="col-12 col-lg-6">
					<form action="{{ route('name_update') }}" method="POST" class="profile__form">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-12">
								<h4 class="profile__title">Profile details</h4>
							</div>

							<div class="col-12 ">
								<div class="profile__group">
									<label class="profile__label" for="username">Email</label>
									<input type="text" class="profile__input" value="{{$user->email}}" disabled>
								</div>
							</div>

							<div class="col-12 ">
								<div class="profile__group">
									<label class="profile__label" for="firstname">First Name</label>
									<input id="firstname" type="text" name="firstname" class="profile__input" value="{{$user->name}}">
								</div>
							</div>

							<div class="col-12">
								<button class="profile__btn" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>
				<!-- end details form -->

				<!-- password form -->
				<div class="col-12 col-lg-6">
					<form method="POST" action="{{ route('changePassword') }}" class="profile__form">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-12">
								<h4 class="profile__title">Change password</h4>
							</div>

							<div class="col-12">
							@if (session('error'))
		                        <div class="alert alert-danger">
		                            {{ session('error') }}
		                        </div>
		                    @endif
	                        @if (session('success'))
	                            <div class="alert alert-success">
	                                {{ session('success') }}
	                            </div>
	                        @endif
	                    	</div>

							<div class="col-12 col-md-6 col-lg-12 col-xl-6">
								<div class="profile__group">
									<label class="profile__label" for="oldpass">Old Password</label>

									<input id="current-password" type="password" class="profile__input" name="current-password" required>

	                                @if ($errors->has('current-password'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('current-password') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-12 col-xl-6">
								<div class="profile__group">
									<label class="profile__label" for="newpass">New Password</label>

									<input id="new-password" type="password" class="profile__input" name="new-password" required>

	                                @if ($errors->has('new-password'))
	                                    <span class="help-block">
	                                        <strong>{{ $errors->first('new-password') }}</strong>
	                                    </span>
	                                @endif
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-12 col-xl-6">
								<div class="profile__group">
									<label class="profile__label" for="confirmpass">Confirm New Password</label>

									<input id="new-password-confirm" type="password" class="profile__input" name="new-password_confirmation" required>
								</div>
							</div>


							<div class="col-12">
								<button class="profile__btn" type="submit">Change</button>
							</div>
						</div>
					</form>
				</div>
				<!-- end password form -->
			</div>
			<div class="row">
				<!-- details form -->
				<div class="col-12 col-lg-12">
					<form action="#" class="profile__form">
						<div class="row">
							<div class="col-12">
								<h4 class="profile__title">Billing Details</h4>
							</div>

							<div class="col-12 col-md-6 col-lg-12 col-xl-6">
								<div class="profile__group">
									<label class="profile__label" for="username">Email</label>
									<input type="text" class="profile__input" value="{{$user->email}}" disabled>
								</div>
							</div>

							<div class="col-12 col-md-6 col-lg-12 col-xl-6">
								<div class="profile__group">
									<label class="profile__label" for="email">Your next billing date</label>
									<input type="text" class="profile__input" value="{{$next_billing}}" disabled>
								</div>
							</div>

							<div class="col-12">
								<h4 class="profile__title">Card Details</h4>
							</div>

							<div class="col-12 col-md-3 col-lg-12 col-xl-3">
								<div class="profile__group">
									<label class="profile__label" for="firstname">Card Number</label>
									<input type="text" class="profile__input" value="****{{$card->last4}}" disabled>
								</div>
							</div>

							<div class="col-12 col-md-3 col-lg-12 col-xl-3">
								<div class="profile__group">
									<label class="profile__label" for="firstname">Expiring (MM/YY)</label>
									<input type="text" class="profile__input" value="{{$card->exp_month}} / {{$card->exp_year}}" disabled>
								</div>
							</div>

							<div class="col-12 col-md-3 col-lg-12 col-xl-3">
								<div class="profile__group">
									<label class="profile__label" for="firstname">Card</label>
									<input type="text" class="profile__input" value="{{$card->brand}}" disabled>
								</div>
							</div>

							<div class="col-12 col-md-3 col-lg-12 col-xl-3">
								<div class="profile__group">
									<label class="profile__label" for="firstname">Country</label>
									<input type="text" class="profile__input" value="{{$card->country}}" disabled>
								</div>
							</div>

							{{-- <div class="col-12 col-md-3 col-lg-2 col-xl-2" >
								<button class="profile__btn" type="button" style="width: 100%">Change Card Details</button>
							</div>
							<div class="col-12 col-md-3 col-lg-2 col-xl-2" >
								<button class="profile__btn" type="button" style="width: 100%">Change Plan</button>
							</div> --}}
						</div>
					</form>
				</div>
				<!-- end details form -->
			</div>


			<!-- end content tabs -->
		</div>
	</div>
	<!-- end content -->


</script>	

@endsection