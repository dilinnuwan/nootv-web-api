@extends('layouts.userlayout')

@section('content')
{{-- {{dd($series->name)}} --}}
<!-- page title -->
<section class="section section--first section--bg" data-bg="{{ asset('img/section/section.jpg') }} ">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__wrap">
					<!-- section title -->
					<h2 class="section__title">{{$series->name}}</h2>
					<!-- end section title -->

					<!-- breadcrumb -->
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{ route('home') }}">Home</a></li>
						<li class="breadcrumb__item"><a href="/channel/{{$series->Channel->id}}">{{$series->Channel->channel_name}}</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">{{$series->name}}</li>
					</ul>
					<!-- end breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end page title -->

<!-- catalog -->
<div class="catalog">
	<div class="container">
		<div class="row">


			@if(!$episodes->isEmpty())
				@foreach($episodes as $episode)
				<!-- card -->
				<div class="col-6 col-sm-3 col-md-3 col-xl-3">
					<div class="card">
						<div class="card__cover">
							<img src="{{$episode->thumb or asset('img/sample_thumb.jpg') }}" alt="">
							<a href="/video/{{$episode->id}}" class="card__play">
								<i class="icon ion-ios-play"></i>
							</a>
						</div>
						<div class="card__content">
							<h3 class="card__title"><a href="/video/{{$episode->id}}">{{$episode->name}} ({{$episode->epsode_id}})</a></h3>
						</div>
						<span class="card__category" style="color:#ccc;font-size: 12px;padding-top: 0px">
							{{ \Carbon\Carbon::createFromTimeStamp($episode->release_date) -> toFormattedDateString() }}
						</span>
					</div>
				</div>
				<!-- end card -->
				@endforeach
			@else
				<div class="col-12 centered">
					<span style="color:#ccc;font-size: 20px">No Results</span>
				</div>
				
			@endif



			{{ $episodes->links() }}

			
		</div>
	</div>
</div>
<!-- end catalog -->

@endsection