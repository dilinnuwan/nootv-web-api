@extends('layouts.userlayout')

@section('content')

<!-- page title -->
<section class="section section--first section--bg" data-bg="{{ asset('img/section/section.jpg') }}  ">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section__wrap">
					<!-- section title -->
					<h2 class="section__title">{{$video->name}}</h2>
					<!-- end section title -->

					<!-- breadcrumb -->
					<ul class="breadcrumb">
						<li class="breadcrumb__item"><a href="{{ route('home') }}">Home</a></li>
						<li class="breadcrumb__item"><a href="/channel/{{$video->Channel->id}}">{{$video->Channel->channel_name}}</a></li>
						<li class="breadcrumb__item"><a href="/channel/{{ $video->Channel->id }}/{{$video->series_id}}">{{$video->Series->name}}</a></li>
						<li class="breadcrumb__item breadcrumb__item--active">{{$video->name}}</li>
					</ul>
					<!-- end breadcrumb -->
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end page title -->

<!-- details -->
<section class="section" data-bg="img/section/details.jpg">
	<!-- details content -->
	<div class="container">
		<div class="row">


			<!-- player -->

			<div class="col-12 col-lg-12">
				<video controls crossorigin playsinline poster="{{$video->thumb}}" id="player">
					<!-- Video files -->
					<source src="{{$video->url}}" type="video/mp4" size="720">

					<!-- Fallback for browsers that don't support the <video> element -->
					{{-- <a href="https://cdn.plyr.io/static/demo/View_From_A_Blue_Moon_Trailer-576p.mp4" download>Download</a> --}}
				</video>
			</div>
			<!-- end player -->
		</div>
	</div>
	<!-- end details content -->
</section>
<!-- end details -->


<!-- catalog -->
<div class="catalog">
	<div class="container">
		<div class="row">

			<div class="col-12">
				<h2 class="content__title">More Episodes</h2>
			</div>


			@if(!$episodes->isEmpty())
				@foreach($episodes as $episode)
				<!-- card -->
				<div class="col-6 col-sm-3 col-md-3 col-xl-3">
					<div class="card">
						<div class="card__cover">
							<img src="{{$episode->thumb or asset('img/sample_thumb.jpg') }}" alt="">
							<a href="/video/{{$episode->id}}" class="card__play">
								<i class="icon ion-ios-play"></i>
							</a>
						</div>
						<div class="card__content">
							<h3 class="card__title"><a href="/video/{{$episode->id}}">{{$episode->name}} ({{$episode->epsode_id}})</a></h3>
						</div>
						<span class="card__category" style="color:#ccc;font-size: 12px;padding-top: 0px">
							{{ \Carbon\Carbon::createFromTimeStamp($episode->release_date) -> toFormattedDateString() }}
						</span>
					</div>
				</div>
				<!-- end card -->
				@endforeach
			@else
				<div class="col-12 centered">
					<span style="color:#ccc;font-size: 20px">No Results</span>
				</div>
				
			@endif
			<!-- end paginator -->
		</div>
	</div>
</div>
<!-- end catalog -->


@endsection



