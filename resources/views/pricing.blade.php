@extends('layouts.mainlayout')

@section('content')
<!-- home -->

<!-- page title -->
<section class="section section--first section--bg" data-bg="img/section/section.jpg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__wrap">
                    <!-- section title -->
                    <h2 class="section__title">Pricing plan</h2>
                    <!-- end section title -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end page title -->

<!-- pricing -->
<div class="section">
    <div class="container">
        <div class="row">
            <!-- plan features -->

            <!-- end plan features -->

            <!-- price -->
            <div class="col-12 col-md-6 col-lg-4">
            </div>
            <!-- end price -->

            <!-- price -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="price price">
                    <div class="price__item price__item--first"><span>Per Month</span><span>$7.99</span></div>
                    <div class="price__item"><span>First month free</span></div>
                    <div class="price__item"><span>Full HD</span></div>
                    <div class="price__item"><span>TV & Desktop</span></div>
                    <div class="price__item"><span>24/7 Support</span></div>
                    <a href="{{ route('register') }}" class="price__btn">Choose Plan</a>
                </div>
            </div>
            <!-- end price -->

            <!-- price -->
            <div class="col-12 col-md-6 col-lg-4">
            </div>
            <!-- end price -->
        </div>
    </div>
</div>
<!-- end pricing -->


<!-- partners -->
<section class="section section--grid section--border">
    <div class="container">
        <div class="row">
            <!-- section title -->
            <div class="col-12">
                <h2 class="section__title section__title--no-margin">Our Partners</h2>
            </div>
            <!-- end section title -->

            <!-- section text -->
            <div class="col-12">
                <p class="section__text section__text--last-with-margin">It is a long <b>established</b> fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using.</p>
            </div>
            <!-- end section text -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/themeforest-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/audiojungle-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/codecanyon-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/photodune-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/activeden-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->

            <!-- partner -->
            <div class="col-6 col-sm-4 col-md-3 col-lg-2">
                <a href="pricing.html#" class="partner">
                    <img src="img/partners/3docean-light-background.png" alt="" class="partner__img">
                </a>
            </div>
            <!-- end partner -->
        </div>
    </div>
</section>
<!-- end partners -->



@endsection