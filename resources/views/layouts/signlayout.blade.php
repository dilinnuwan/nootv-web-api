<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
   @include('layouts.partials.head')
</head>

<body class="body">

	@yield('content')
	@include('layouts.partials.footer')
	@include('layouts.partials.footer-scripts')

	
</body>
</html>