<!-- footer -->
<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="footer__content">
					<a href="/" class="footer__logo">
						<img src="{{ asset('img/logo-01.svg') }}" alt="">
					</a>

					<span class="footer__copyright">© NOTV SriLanka<br></span>

					<nav class="footer__nav">
						<a href="about.html">About Us</a>
						<a href="contacts.html">Contacts</a>
						<a href="privacy.html">Privacy Policy</a>
					</nav>

					<button class="footer__back" type="button">
						<i class="icon ion-ios-arrow-round-up"></i>
					</button>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- end footer -->