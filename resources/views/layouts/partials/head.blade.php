<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">


<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- CSS -->
<link rel="stylesheet" href="{{ asset('css/bootstrap-reboot.min.css') }}">
{{-- <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('css/bootstrap-grid.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/nouislider.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/plyr.css') }}">
<link rel="stylesheet" href="{{ asset('css/photoswipe.css') }}">
<link rel="stylesheet" href="{{ asset('css/default-skin.css') }}">
<link rel="stylesheet" href="{{ asset('css/main.css') }}">

<!-- Favicons -->
<link rel="icon" type="image/png" href="{{ asset('icon/favicon-32x32.png') }} " sizes="32x32">
<link rel="apple-touch-icon" href="{{ asset('icon/favicon-32x32.png') }} ">
<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('icon/apple-touch-icon-72x72.png') }} ">
<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('icon/apple-touch-icon-114x114.png') }} ">
<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('icon/apple-touch-icon-144x144.png') }} ">

<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="DG">
<title>{{ config('app.name', 'Laravel') }}  - Online SriLankan TV Series, Movies and More!</title>