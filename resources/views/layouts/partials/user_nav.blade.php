<!-- header -->
<header class="header">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="header__content">
					<!-- header logo -->
					<a href="/" class="header__logo">
						<img src="{{ asset('img/logo-01.svg') }}" alt="">
					</a>
					<!-- end header logo -->

					<!-- header nav -->
					<ul class="header__nav">

						<li class="header__nav-item">
							<a href="/" class="header__nav-link">Home</a>
						</li>
						<li class="header__nav-item">
							<a href="/search" class="header__nav-link">Search</a>
						</li>
					</ul>
					<!-- end header nav -->

					<!-- header auth -->
					<div class="header__auth">

						{{-- <a href="{{ route('login') }}" class="header__sign-in">
							<i class="icon ion-ios-log-in"></i>
							<span>sign in</span>
						</a> --}}
						<div class="dropdown header__lang">
							<a class="dropdown-toggle header__nav-link" href="profile.html#" role="button" id="dropdownMenuLang" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}}</a>

							<ul class="dropdown-menu header__dropdown-menu" aria-labelledby="dropdownMenuLang">
								<li><a href="/account">Account</a></li>
								<li>
									<a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
								</li>
							</ul>
						</div>
					</div>
					<!-- end header auth -->

					<!-- header menu btn -->
					<button class="header__btn" type="button">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<!-- end header menu btn -->
				</div>
			</div>
		</div>
	</div>
</header>
<!-- end header -->