<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
   @include('layouts.partials.head')
   @yield('header')
</head>

<body class="body">

	@include('layouts.partials.user_nav')
	@yield('content')
	@include('layouts.partials.footer')

	@yield('footer')
	@include('layouts.partials.footer-scripts')

	
</body>
</html>