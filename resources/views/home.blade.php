{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}


@extends('layouts.userlayout')

@section('content')
<!-- home -->
<section class="home">
    <!-- home bg -->
    <div class="owl-carousel home__bg">
        <div class="item home__cover" data-bg="img/home/home__bg.jpg"></div>
        <div class="item home__cover" data-bg="img/home/home__bg2.jpg"></div>
        <div class="item home__cover" data-bg="img/home/home__bg3.jpg"></div>
        <div class="item home__cover" data-bg="img/home/home__bg4.jpg"></div>
        <div class="item home__cover" data-bg="img/home/home__bg5.jpg"></div>
    </div>
    <!-- end home bg -->

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1 class="home__title"><b>TV</b> Channels</h1>

                <button class="home__nav home__nav--prev" type="button">
                    <i class="icon ion-ios-arrow-round-back"></i>
                </button>
                <button class="home__nav home__nav--next" type="button">
                    <i class="icon ion-ios-arrow-round-forward"></i>
                </button>
            </div>

            <div class="col-12">
                <div class="owl-carousel home__carousel">

                    @foreach($channels as $channel)

                    <!-- card -->
                    <div class="card card--big">
                        <div class="card__cover">
                            <img src="{{$channel->channel_logo_url}}" alt="">
                            <a href="/channel/{{$channel->id}}" class="card__play">
                                <i class="icon ion-ios-play"></i>
                            </a>
                        </div>
                        <div class="card__content">
                            <h3 class="card__title"><a href="index.html#">{{$channel->channel_name}}</a></h3>
                        </div>
                    </div>
                    <!-- end card -->
                    @endforeach


                </div>
            </div>
        </div>
    </div>
</section>
<!-- end home -->


@endsection