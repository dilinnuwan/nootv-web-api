{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}



@extends('layouts.signlayout')

@section('content')

<!-- section -->
<div class="sign section--bg" data-bg="{{ asset('img/section/section.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sign__content">
                    <!-- authorization form -->
                    <form class="sign__form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <a href="/" class="sign__logo">
                            <img src="{{ asset('img/logo-01.svg')}}" alt="">
                        </a>

                        
                        <div class="sign__group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="sign__input" name="email" placeholder="Email" value="{{ old('email') }}"  required>

                            @if ($errors->has('email'))
                                <span class="help-block" style="color:#aaa">
                                   {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                        
                        <button class="sign__btn" type="submit">Send Password Reset Link</button>

                        <span class="sign__text">Don't have an account? <a href="{{ route('register') }}">Sign up!</a></span>

                        <span class="sign__text"><a href="{{ route('login') }}">Sign In</a></span>
                    </form>
                    <!-- end authorization form -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end section -->


@endsection