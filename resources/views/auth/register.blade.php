{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}



@extends('layouts.signlayout')

@section('content')

<!-- section -->
{{-- <div class="sign section--bg" data-bg="img/section/section.jpg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sign__content">
                    <!-- authorization form -->
                    <form class="sign__form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <a href="/" class="sign__logo">
                            <img src="img/logo-01.svg" alt="">
                        </a>

                        <div class="sign__group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input id="email" type="email" class="sign__input" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="help-block" style="color:#aaa">
                                   {{ $errors->first('email') }}
                                </span>
                            @endif
                        </div>
                        <div class="sign__group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="sign__input" placeholder="Password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block" style="color:#aaa">
                                       {{ $errors->first('password') }}
                                    </span>
                                @endif
                        </div>

                        <div class="sign__group sign__group--checkbox">
                            <input id="remember" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember">Remember Me</label>
                        </div>
                        
                        <button class="sign__btn" type="submit">Sign in</button>

                        <span class="sign__text">Don't have an account? <a href="{{ route('register') }}">Sign up!</a></span>

                        <span class="sign__text"><a href="{{ route('password.request') }}">Forgot password?</a></span>
                    </form>
                    <!-- end authorization form -->
                </div>
            </div>
        </div>
    </div>
</div> --}}
<!-- end section -->


<div class="sign section--bg" data-bg="{{ asset('img/section/section.jpg') }}">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sign__content">
                    <!-- registration form -->
                    <form class="sign__form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <a href="/" class="sign__logo">
                            <img src="{{ asset('img/logo-01.svg') }}" alt="">
                        </a>

                        

                        <div class="sign__group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input id="name" type="text" class="sign__input" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block" style="color:#aaa">
                                       {{ $errors->first('name') }}
                                    </span>
                                @endif
                        </div>

                        <div class="sign__group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="sign__input" name="email" value="{{ old('email') }}" placeholder="Email" required>

                                @if ($errors->has('email'))
                                    <span class="help-block" style="color:#aaa">
                                       {{ $errors->first('email') }}
                                    </span>
                                @endif
                        </div>

                        <div class="sign__group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <input id="password" type="password" class="sign__input" name="password" placeholder="Password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block" style="color:#aaa">
                                       {{ $errors->first('password') }}
                                    </span>
                                @endif
                        </div>

                        <div class="sign__group">
                                <input id="password-confirm" type="password" class="sign__input" name="password_confirmation" placeholder="Password Confirmation" required>
                        </div>


                        <div class="sign__group sign__group--checkbox">
                            <input id="remember" name="remember" type="checkbox" checked="checked">
                            <label for="remember">I agree to the <a href="privacy.html">Privacy Policy</a></label>
                        </div>
                        
                        <button class="sign__btn" type="submit">Sign up</button>

                        <span class="sign__text">Already have an account? <a href="{{ route('login') }}">Sign in!</a></span>
                    </form>
                    <!-- registration form -->
                </div>
            </div>
        </div>
    </div>
</div>

@endsection







